CXX=clang++
CXXFLAGS=-Wall -Werror -std=c++11 -g3
CPPFLAGS=-MMD `pkg-config --cflags opencv`
LDFLAGS=`pkg-config --libs opencv`
BIN=barcode
SRC=$(addprefix src/, main.cc cluster.cc barcode.cc barcodes.cc)
OBJS=$(SRC:.cc=.o)
DEPS=$(OBJS:.o=.d)

all: $(BIN)

$(BIN): $(OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@

clean:
	$(RM) $(OBJS) $(BIN) $(DEPS)

.PHONY: all clean

-include $(DEPS)
