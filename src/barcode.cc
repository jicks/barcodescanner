#include <cmath>
#include "barcode.hh"

/*
 * Trys to extract a valid barcode from the given scanned line.
 * The ratio is extracted from the first guard.
 * If any problem is encountered during the extraction or if the checksum is
 * invalid, no valid barcode is considered extracted.
*/
Barcode::Barcode(std::vector<unsigned>& line_)
    : line(line_), ratio(0), pos(0), value(0), valid(false)
{
    if (!getFirstGuard()) return;

    NumberType types[6];
    for (int i = 2; i <= 7; i++)
    {
        std::pair<NumberType, unsigned int> values = getNumber();

        if (values.first == NumberType::Error || values.second > 9)
            return;
        types[i - 2] = values.first;
        value = value * 10 + values.second;
    }

    unsigned int firstNumber = getFirstNumber(types);
    if (firstNumber > 9) return;
    value += firstNumber * 1000000;

    if (!getMiddleGuard()) return;
    for (int i = 8; i <= 13; i++)
    {
        std::pair<NumberType, unsigned int> values = getNumber();

        if (values.first != NumberType::R || values.second > 9)
            return;
        value = value * 10 + values.second;
    }
    if (!getLastGuard()) return;
    if (!checksum()) return;

    valid = true;
}

bool Barcode::isValid() const
{
    return valid;
}

unsigned long long int Barcode::getValue() const
{
    return value;
}

/*
 * Returns the next column of the scanned line.
 * Expected input value if 0 for white (even columns) and 1 for black (odd
 * columns).
*/
unsigned int Barcode::getColumn(unsigned char value)
{
    if ((pos % 2) != value)
        return 0;
    if (pos < line.size())
        return line[pos++];
    return 0;
}

/*
 * Trys to extract the first guard and the ratio.
 * Upon failure, try again while the scanned line is not over.
*/
bool Barcode::getFirstGuard()
{
    pos = 1;

    // If first values are wrong, try again with a shift.
    while (pos + 2 < line.size())
    {
        ratio = line[pos];
        unsigned a = line[++pos];
        unsigned b = line[pos + 1];
        if (std::lround((double)a / ratio) == 1 && std::lround((double)b / ratio) == 1)
        {
            pos += 2;
            return true;
        }
    }

    return false;
}

/*
 * Checks whether the next 5 columns of the scanned line are effectively the
 * middle guard of an EAN13.
*/
bool Barcode::getMiddleGuard()
{
    unsigned int values[2];
    getPossibleWidth(getColumn(0), values);
    if (values[0] != 1 && values[1] != 1) return false;
    getPossibleWidth(getColumn(1), values);
    if (values[0] != 1 && values[1] != 1) return false;
    getPossibleWidth(getColumn(0), values);
    if (values[0] != 1 && values[1] != 1) return false;
    getPossibleWidth(getColumn(1), values);
    if (values[0] != 1 && values[1] != 1) return false;
    getPossibleWidth(getColumn(0), values);
    if (values[0] != 1 && values[1] != 1) return false;
    return true;
}

/*
 * Checks whether the next 3 columns of the scanned line are effectively the
 * last guard of an EAN13.
*/
bool Barcode::getLastGuard()
{
    unsigned int values[2];
    getPossibleWidth(getColumn(1), values);
    if (values[0] != 1 && values[1] != 1) return false;
    getPossibleWidth(getColumn(0), values);
    if (values[0] != 1 && values[1] != 1) return false;
    getPossibleWidth(getColumn(1), values);
    if (values[0] != 1 && values[1] != 1) return false;
    return true;
}

void Barcode::getPossibleWidth(unsigned int column, unsigned int* table) const
{
    double a = (double)column / ratio;
    table[0] = std::lround(a);
    table[1] = (table[0] > a) ? std::lround(std::floor(a)) : std::lround(std::ceil(a));
}

unsigned int Barcode::getNumber(unsigned int a, unsigned int b,
                                unsigned int c, unsigned int d) const
{
    if (a > 9 || b > 9 || d > 9 || d > 9)
        return -1;
    unsigned int number = a * 1000 + b * 100 + c * 10 + d;
    switch (number)
    {
        case 3211: case 1123:
            return 0;
        case 2221: case 1222:
            return 1;
        case 2122: case 2212:
            return 2;
        case 1411: case 1141:
            return 3;
        case 1132: case 2311:
            return 4;
        case 1231: case 1321:
            return 5;
        case 1114: case 4111:
            return 6;
        case 1312: case 2131:
            return 7;
        case 1213: case 3121:
            return 8;
        case 3112: case 2113:
            return 9;
        default:
            return -1;
    }
}

/*
 * Extracts the next number of the scanned line and its corresponding type.
 * If the next values of the scanned line aren't a valid number, we try
 * rounding the values the other way. If it still isn't valid, an error is
 * returned.
*/
std::pair<NumberType, unsigned int> Barcode::getNumber()
{
    if (pos >= line.size())
        return std::make_pair(NumberType::Error, -1);
    char value = (pos % 2); // First one is white, so even values are white

    static unsigned int order[16][4] =
        {
            {0,0,0,0}, {0,0,0,1}, {0,0,1,0}, {0,1,0,0},
            {1,0,0,0}, {0,0,1,1}, {0,1,0,1}, {1,0,0,1},
            {0,1,1,0}, {1,0,1,0}, {1,1,0,0}, {0,1,1,1},
            {1,0,1,1}, {1,1,0,1}, {1,1,1,0}, {1,1,1,1}
        };

    unsigned int aa[2]; getPossibleWidth(getColumn(value), aa);
    unsigned int bb[2]; getPossibleWidth(getColumn(1 - value), bb);
    unsigned int cc[2]; getPossibleWidth(getColumn(value), cc);
    unsigned int dd[2]; getPossibleWidth(getColumn(1 - value), dd);

    for (unsigned int i = 0; i < 16; i++)
    {
        unsigned int a = aa[order[i][0]];
        unsigned int b = bb[order[i][1]];
        unsigned int c = cc[order[i][2]];
        unsigned int d = dd[order[i][3]];

        bool evenParity = ((value == 1) ? a + c : b + d) % 2 != 1;
        NumberType type;
        if (evenParity)
        {
            if (value == 1) type = NumberType::R;
            else type = NumberType::G;
        } else
        {
            if (value == 1) type = NumberType::Error;
            else type = NumberType::L;
        }

        unsigned int number = getNumber(a, b, c, d);
        if (type == NumberType::Error || number > 9)
            continue;

        return std::make_pair(type, number);
    }
    return std::make_pair(NumberType::Error, -1);
}

/*
 * Trys to guess the first number of the barcode given the types of the six
 * following numbers.
 * If their types are not compatible (for an EAN13), an invalid number is
 * returned.
*/
unsigned int Barcode::getFirstNumber(NumberType types[6]) const
{
    unsigned int value = (1<<5)*types[0] + (1<<4)*types[1] + (1<<3)*types[2] +
                         (1<<2)*types[3] + (1<<1)*types[4] + (1<<0)*types[5];

    switch (value)
    {
        case 0: // LLLLLL
            return 0;
        case 11: // LLGLGG
            return 1;
        case 13: // LLGGLG
            return 2;
        case 14: // LLGGGL
            return 3;
        case 19: // LGLLGG
            return 4;
        case 25: // LGGLLG
            return 5;
        case 28: // LGGGLL
            return 6;
        case 21: // LGLGLG
            return 7;
        case 22: // LGLGGL
            return 8;
        case 26: // LGGLGL
            return 9;
        default:
            return -1;
    }
}

/*
 * Returns true if the checksum of the extracted barcode is valid.
*/
bool Barcode::checksum() const
{
    unsigned long long int number = value / 10;
    unsigned int checksum = 0;
    for (int i = 1; i <= 12; i++)
    {
        unsigned int weigth = (i % 2 == 1) ? 3 : 1;
        checksum += (number % 10) * weigth;
        number /= 10;
    }

    return (10 - (checksum % 10)) % 10 == (value % 10);
}
