#ifndef BARCODE_HH_

# define BARCODE_HH_

# include <vector>
# include <utility>

enum NumberType
{
    L = 0,
    G = 1,
    R = 2,
    Error = 3
};

class Barcode
{
    public:
        Barcode(std::vector<unsigned>& line);
        bool isValid() const;
        unsigned long long int getValue() const;
    private:
        unsigned int getColumn(unsigned char value);
        bool getFirstGuard();
        bool getMiddleGuard();
        bool getLastGuard();
        std::pair<NumberType, unsigned int> getNumber();
        void getPossibleWidth(unsigned int column, unsigned int* table) const;
        unsigned int getNumber(unsigned int a, unsigned int b,
                               unsigned int c, unsigned int d) const;
        unsigned int getFirstNumber(NumberType types[6]) const;
        bool checksum() const;

        std::vector<unsigned>& line;
        unsigned int ratio;
        unsigned int pos;
        unsigned long long int value;
        bool valid;
};

#endif /* !BARCODE_HH_ */
