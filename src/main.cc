#include <algorithm>
#include <cv.h>
#include <highgui.h>
#include <cmath>
#include <functional>
#include <cstdio>
#include <vector>

#include "barcodes.hh"
#include "cluster.hh"


int main(int argc, char** argv)
{
    cv::Mat src, dst;
    if(argc != 2 || !(src = cv::imread(argv[1])).data)
        return -1;

    // Enhancing the constrast of the input image
    double alpha = 1.2;
    double beta = 10;
    cv::cvtColor(src, dst, CV_BGR2GRAY);
    cv::equalizeHist(dst, dst);
    for (int y = 0; y < dst.rows; y++)
        for (int x = 0; x < dst.cols; x++)
            dst.at<uchar>(y, x) = cv::saturate_cast<uchar>(alpha * dst.at<uchar>(y, x) + beta);


    // Black Hat method to extract area with high constrast
    cv::morphologyEx(dst, dst, cv::MORPH_BLACKHAT,
            cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2,2)),
            cv::Point(-1,-1), 1);

    // Canny Edge detection
    cv::Canny(dst, dst, 150, 250, 3);

    std::vector<cv::Vec4i> lines;
    // Hough transform to detect lines
    cv::HoughLinesP(dst, lines, 1, CV_PI/180, 80, 30, 10);
    cv::cvtColor(dst, dst, CV_GRAY2BGR);

    std::vector<Cluster> clusters = getClusters(lines);
    std::sort(clusters.begin(), clusters.end(), std::greater<Cluster>());

    for (size_t i = 0; i < clusters.size() && i < 10; i++)
    {
        clusters[i].computeBoundingBox();
        clusters[i].displayBoundingBox(dst);
        clusters[i].displayLines(dst, i);

        Barcodes barcodes;
        std::vector<std::vector<unsigned>> lines = clusters[i].getScanlines(src);
        for (size_t j = 0; j < lines.size(); j++)
            barcodes.addLine(lines[j]);

        if (barcodes.isValid())
        {
            clusters[i].displayBoundingBox(src);
            printf("Barcode (EAN 13): %010llu\n", barcodes.getMostProbable());
        }
    }

    double ratio = std::min(1280.0/src.size().width, 720.0/src.size().height);
    cv::resize(src, src, cv::Size(0, 0), ratio, ratio);
    cv::namedWindow("Barcode Scanner", 1);
    cv::imshow("Barcode Scanner", src);

    while (cv::waitKey(0) != 27);
    return 0;
}
