#include <algorithm>
#include <cmath>
#include "cluster.hh"

#define ANGLE_DIFFERENCE (5.0)

Cluster::Cluster(const cv::Vec4i& line)
    : count(1), angle(0.0), box(nullptr)
{
    points.push_back(cv::Point2f(line[0], line[1]));
    points.push_back(cv::Point2f(line[2], line[3]));
    angle = std::atan2(line[3] - line[1], line[2] - line[0]) + CV_PI;
    angle = std::fmod(angle, CV_PI) * 180 / CV_PI;
    minPos = points[points.size() - 2];
    maxPos = points[points.size() - 1];;
    count = cv::norm(minPos - maxPos);
}

Cluster::~Cluster()
{
    delete box;
}

/*
 * Projects the point P on the line segment AB and gets its coordinate.
*/
static double projectedPosition(const cv::Point2f& P,
                                const cv::Point2f& A, const cv::Point2f& B)
{
    double ratio = 1 / std::sqrt(std::pow(B.x - A.x, 2) + std::pow(B.y - A.y, 2));
    cv::Point2f U((B.x - A.x) * ratio, (B.y - A.y) * ratio);

    return (P.x - A.x) * U.x + (P.y - A.y) * U.y;
}

/*
 * Set the minimum/maximum projected positions between two clusters.
*/
void Cluster::setProjectedPositions(Cluster& cluster)
{
    double pPos = projectedPosition(cluster.minPos, minPos, maxPos);
    double qPos = projectedPosition(cluster.maxPos, minPos, maxPos);
    double aPos = projectedPosition(minPos, minPos, maxPos);
    double bPos = projectedPosition(maxPos, minPos, maxPos);

    if (pPos < qPos && pPos < aPos) minPos = cluster.minPos;
    else if (qPos < aPos) minPos = cluster.maxPos;
    if (pPos > qPos && pPos > bPos) maxPos = cluster.minPos;
    else if (qPos > bPos) maxPos = cluster.maxPos;
}

/*
 * Fuses together two clusters.
 * The new angle is computed as the mean of the two clusters.
*/
void Cluster::fuse(Cluster& cluster)
{
    if (box)
    {
        delete box;
        box = nullptr;
    }
    if (cluster.box)
    {
        delete cluster.box;
        cluster.box = nullptr;
    }
    angle = (angle * count + cluster.angle * cluster.count) / (count + cluster.count);
    count += cluster.count;
    points.insert(points.end(), cluster.points.begin(), cluster.points.end());
    setProjectedPositions(cluster);
}

/*
 * Checks whether two clusters should be fused together.
 * Two different points are considered:
 * - Are the angle of the clusters close enough (currently <5°)?
 * - Is the distance between the nearest lines small enough (currently < 1/3 of
 *   the line length)?
*/
bool Cluster::shouldFuse(const Cluster& cluster)
{
    if (std::fabs(angle - cluster.angle) > ANGLE_DIFFERENCE)
        return false;

    if (std::min(projectedPosition(maxPos, minPos, maxPos),
             projectedPosition(cluster.maxPos, minPos, maxPos)) -
        std::max(0.0, projectedPosition(cluster.minPos, minPos, maxPos)) < 0)
        return false;

    for (size_t i = 0; i < points.size(); i += 2)
    {
        for (size_t j = 0; j < cluster.points.size(); j += 2)
        {
            double lengthA = cv::norm(points[i] - points[i + 1]);
            double lengthB = cv::norm(cluster.points[j] - cluster.points[j + 1]);
            double distance = std::min({cv::norm(points[i] - cluster.points[j]),
                                        cv::norm(points[i] - cluster.points[j + 1]),
                                        cv::norm(points[i + 1] - cluster.points[j]),
                                        cv::norm(points[i + 1] - cluster.points[j + 1])});

            if (3 * distance < std::max(lengthA, lengthB))
                return true;
        }
    }

    return false;
}

/*
 * Computes the bounding box of the lines in the cluster.
*/
void Cluster::computeBoundingBox()
{
    if (box)
        return;
    box = new cv::RotatedRect();
    *box = cv::minAreaRect(cv::Mat(points));
}

static cv::Scalar getColor(size_t i)
{
    static cv::Scalar colors[] = {
        cv::Scalar(0, 0, 255),
        cv::Scalar(0, 255, 0),
        cv::Scalar(0, 255, 255),
        cv::Scalar(255, 0, 0),
        cv::Scalar(255, 0, 255),
        cv::Scalar(255, 255, 0),
        cv::Scalar(0, 0, 128),
        cv::Scalar(0, 128, 0),
        cv::Scalar(0, 128, 128),
        cv::Scalar(128, 0, 0),
        cv::Scalar(128, 0, 128),
        cv::Scalar(128, 128, 0),

    };
    return colors[i % (sizeof (colors) / sizeof (colors[0]))];
}

/*
 * Displays the bounding box of the lines in the cluster.
 * It also displays one of the scanned lines.
*/
void Cluster::displayBoundingBox(cv::Mat& img) const
{
    if (!box) return;

    cv::Point2f vertices[4];
    box->points(vertices);
    for(size_t i = 0; i < 4; ++i)
        cv::line(img, vertices[i], vertices[(i + 1) % 4], getColor(0), 4, CV_AA);

    double lineSize = std::max(box->size.height, box->size.width) * 1.2;
    double stepSize = std::min(box->size.height, box->size.width);
    double a = lineSize * std::sin(angle * CV_PI / 180.0);
    double b = lineSize * std::cos(angle * CV_PI / 180.0);

    double stepX = stepSize * std::cos(angle * CV_PI / 180.0) / 10;
    double stepY = stepSize * std::sin(angle * CV_PI / 180.0) / 10;

    int n = 0;

    cv::Point x(box->center.x - a/2 + stepX * n,
                box->center.y + b/2 + stepY * n);
    cv::Point y(box->center.x + a/2 + stepX * n,
                box->center.y - b/2 + stepY * n);

    cv::line(img, x, y, getColor(0), 5, CV_AA);
}

void Cluster::displayLines(cv::Mat& img, size_t n) const
{
    for (size_t i = 0; i < points.size(); i += 2)
        cv::line(img, points[i], points[i + 1], cv::Scalar(255,255,255), 3, 8);
}

static unsigned char grayscale(const cv::Vec3b& p)
{
    return (unsigned char)(p.val[0] * 0.299 + p.val[1] * 0.587 + p.val[2] * 0.114);
}

/*
 * Extracts one of the scanned line, given a threshold (for binarizarion).
 * The returned scanned line is the alternation of the width of the white and
 * black bars found.
*/
std::vector<unsigned> Cluster::getScanline(const cv::Mat& img, int n,
        unsigned char threshold, bool reverse=false) const
{
    if (!box) std::vector<cv::Vec3b>();

    double lineSize = std::max(box->size.height, box->size.width) * 1.2;
    double stepSize = std::min(box->size.height, box->size.width);
    double a = lineSize * std::sin(angle * CV_PI / 180.0);
    double b = lineSize * std::cos(angle * CV_PI / 180.0);

    double stepX = stepSize * std::cos(angle * CV_PI / 180.0) / 10;
    double stepY = stepSize * std::sin(angle * CV_PI / 180.0) / 10;

    cv::Point x(box->center.x - a/2 + stepX * n,
                box->center.y + b/2 + stepY * n);
    cv::Point y(box->center.x + a/2 + stepX * n,
                box->center.y - b/2 + stepY * n);

    if (reverse)
        std::swap(x, y);

    cv::LineIterator it (img, x, y, 8);
    std::vector<unsigned> line(1, 1); // First element is ALWAYS white.
    bool last = false;

    for(size_t i = 0; i < it.count; i++, ++it)
    {
        bool current = (grayscale(*(const cv::Vec3b*)*it) <= threshold);
        if (current == last)
            line[line.size() - 1] += 1;
        else
        {
            line.push_back(1);
            last = current;
        }
    }

    return line;
}

/*
 * Returns all the scanned lines of a cluster.
*/
std::vector<std::vector<unsigned>> Cluster::getScanlines(const cv::Mat& img) const
{
    std::vector<std::vector<unsigned>> lines;
    lines.reserve(2 * 11 * 14);

    for (int n = -5; n <= 5; n++)
    {
        for (unsigned char threshold = 32; threshold <= 224; threshold += 16)
        {
            lines.push_back(getScanline(img, n, threshold));
            lines.push_back(getScanline(img, n, threshold, true));
        }
    }

    return lines;
}

bool Cluster::operator >(const Cluster& cluster) const
{
    return count > cluster.count;
}

/*
 * Creates the clusters and fuses them together if necessary.
*/
std::vector<Cluster> getClusters(const std::vector<cv::Vec4i>& lines)
{
    std::vector<Cluster> clusters;
    for (cv::Vec4i line : lines)
        clusters.push_back(Cluster(line));

    bool change = true;
    while (change)
    {
        change = false;
        for (size_t i = 0; i < clusters.size(); i++)
        {
            for (size_t j = i + 1; j < clusters.size(); j++)
            {
                if (!clusters[i].shouldFuse(clusters[j]))
                    continue;

                clusters[i].fuse(clusters[j]);
                if (j + 1 <= clusters.size())
                    clusters[j] = clusters[clusters.size() - 1];
                clusters.pop_back();
                j--;
                change = true;
            }
        }
    }

    return clusters;
}
