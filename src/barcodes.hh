#ifndef BARCODES_HH_

# define BARCODES_HH_

# include <map>
# include <vector>

class Barcodes
{
    public:
        bool isValid() const;
        unsigned long long int getMostProbable() const;
        void addLine(std::vector<unsigned>& line);

    private:
        std::map<unsigned long long int, unsigned int> codes;
};

#endif /* !BARCODES_HH_ */
