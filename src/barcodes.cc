#include "barcodes.hh"
#include "barcode.hh"

#define MIN_OCURRENCES (1)

/*
 * Returns whether one (or several) valid barcode has been found in the given
 * scanned lines.
*/
bool Barcodes::isValid() const
{
    for (auto& code: codes)
        if (code.second >= MIN_OCURRENCES)
            return true;

    return false;
}

/*
 * Returns the most probable barcode given several scanned lines.
 * The most probable barcode is the most ocurring one.
 * Assumes the function isValid() has been called and has returned true.
*/
unsigned long long int Barcodes::getMostProbable() const
{
    unsigned long long int maxCode = 0;
    unsigned int maxOccurences = 0;

    for (auto& code: codes)
    {
        if (code.second > maxOccurences)
        {
            maxCode = code.first;
            maxOccurences = code.second;
        }
    }

    return maxCode;
}

/*
 * Trys to extract a barcode from another scanned line.
*/
void Barcodes::addLine(std::vector<unsigned>& line)
{
    Barcode barcode(line);

    if (barcode.isValid())
        codes[barcode.getValue()] += 1;
}


