#ifndef CLUSTER_HH_

# define CLUSTER_HH_

# include <cv.h>
# include <vector>

class Cluster
{
    public:
        Cluster(const cv::Vec4i& line);
        ~Cluster();
        void fuse(Cluster& cluster);
        bool shouldFuse(const Cluster& cluster);
        void computeBoundingBox();
        void displayBoundingBox(cv::Mat& img) const;
        void displayLines(cv::Mat& img, size_t n) const;
        std::vector<unsigned> getScanline(const cv::Mat& img, int n,
                unsigned char threshold, bool reverse) const;
        std::vector<std::vector<unsigned>> getScanlines(const cv::Mat& img) const;
        bool operator >(const Cluster& cluster) const;
    //private:
        void setProjectedPositions(Cluster& cluster);

        std::vector<cv::Point2f> points;
        double count;
        double angle;
        cv::Point2f maxPos;
        cv::Point2f minPos;
        cv::RotatedRect* box;
};

std::vector<Cluster> getClusters(const std::vector<cv::Vec4i>& lines);

#endif /* !CLUSTER_HH_ */
